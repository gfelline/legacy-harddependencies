[Testing and Refactoring Legacy Code](https://youtu.be/_NnElPO5BU0)
============================================

What is it about?
-----------------

Provides an example of existing code that needs to be unit tested. But there is one rule:

> We can't change any existing code if not covered by tests. The only exception is if we need to change the code to add unit tests, but in this case, just automated refactorings (via IDE) are allowed. 

Although this is a very small piece of code, it has a lot of the problems that we find in legacy code. 


Business Requirements
---------------------

Imagine a social networking website for travellers

* You need to be logged in to see the content
* You need to be friend to see someone else's trips

TripService
-----------
```java
public class TripService_Original {

	public List<Trip> getTripsByUser(User user) throws UserNotLoggedInException {
		List<Trip> tripList = new ArrayList<Trip>();
		User loggedUser = UserSession.getInstance().getLoggedUser();
		boolean isFriend = false;
		if (loggedUser != null) {
			for (User friend : user.getFriends()) {
				if (friend.equals(loggedUser)) {
					isFriend = true;
					break;
				}
			}
			if (isFriend) {
				tripList = TripDAO.findTripsByUser(user);
			}
			return tripList;
		} else {
			throw new UserNotLoggedInException();
		}
	}
}
```

Lecacy Code Rules
-----------------
* You cannot change production code if not covered by tests
	* Just automated refactorings (via IDEs) are allowed, if needed to write the test
	
Tips
----
* Use a code coverage tool
	* Use Eclemma
* Use moreunit plugin (if you use eclipse) to switch fast between production 
code and unittest
	* You will find here the zipped update site: moreunit\MoreUnit-Eclipse-3.2.0.zip
	* Eclipse Help -> Install new software -> Add Archive
* Commit as often as possible
	* Enables you to commit (push) improvements quickly
	* Allows you to roll back (reset) if you get lost
* When refactoring, try to stay in the green for as long as possible
* Start testing from shortest to deepest branch
* Start refactoring from deepest to shortest branch
![code-branches.png](images/code-branches.png)


Activity Log
------------

Create a test for TripService
-----------------------------
* Create TripServiceTest (go to Tripservice and type ctrl+j)
* Search in TripService the shortest branch -> Line 28 
* Create the first test <br>
`should_throw_an_exception_when_user_is_not_logged_in()`
* The Test fails -> Why?<br>
The Problem here is the static *UserSession* Class
![iteration-1.png](images/iteration-1.png)
* Try to fix this Problem

Introduce seam method
-------------------------
* create a seam -> extract method -> extract UserSession

```java
public class TripService {
	public List<Trip> getTripsByUser(User user) throws UserNotLoggedInException {
		List<Trip> tripList = new ArrayList<Trip>();
		// Use the seam method
		User loggedUser = getLoggedInUser();
		boolean isFriend = false;
		if (loggedUser != null) {
			for (User friend : user.getFriends()) {
				if (friend.equals(loggedUser)) {
					isFriend = true;
					break;
				}
			}
			if (isFriend) {
				tripList = TripDAO.findTripsByUser(user);
			}
			return tripList;
		} else {
			throw new UserNotLoggedInException();
		}
	}

	// Seam
	protected User getLoggedInUser() {
		return UserSession.getInstance().getLoggedUser();
	}
}
```
* create a TestableTripService that override the seam Method

```java
public class TripServiceTest {
       @Test(expected = UserNotLoggedInException.class)
       public void should_throw_an_exception_when_user_is_not_logged_in() throws Exception {
               // Given
               TripService tripService = new TestableTripService();
               // When
               tripService.getTripsByUser(null);
       }

       private class TestableTripService extends TripService {
               @Override
               protected User getLoggedInUser() {
                       return null;
               }
       }
}
```

* TestableTripService don't use the UserSession Singleton
![iteration-2.png](images/iteration-2.png)



Make the test more readable
-------------------------------------
* Remove hidden parts in the test
* We want see, if a User is not logged in, an exception should be thrown

```java
public class TripServiceTest {
	private static final User GUEST = null;
	private static final User UNUSED_USER = null;
	private User loggedInUser;

	@Test(expected = UserNotLoggedInException.class)
	public void should_throw_an_exception_when_user_is_not_logged_in() throws Exception {
		// Given
		TripService tripService = new TestableTripService();
		loggedInUser = GUEST;
		
		// When
		tripService.getTripsByUser(UNUSED_USER);
	}
	
	private class TestableTripService extends TripService {
		
		@Override
		protected User getLoggedInUser() {
			return loggedInUser;
		}
	}
}
```

Identify with coverage the 2nd shortest branch to test
---------------------------------------------
![2nd-shortest-branch.png](images/2nd-shortest-branch.png)

Create a test for 2nd shortest branch
----------------------------------------------
* Create the second test <br>
`should_not_return_any_trips_when_users_are_not_friends()`

```java
	@Test
	public void should_not_return_any_trips_when_users_are_not_friends() throws Exception {
		// Given
		TripService tripService = new TestableTripService();
		loggedInUser = REGISTRED_USER;

		User friend = new User();
		friend.addFriend(ANOTHER_USER);
		friend.addTrip(TO_BRAZIL);
		
		// When
		List<Trip> friendTrips = tripService.getTripsByUser(friend);
		
		// Then
		assertThat(friendTrips.size(), equalTo(0));
	}
```

Move the creation of tripService to @before
--------------------------
* Move the creation of tripService in each test in a JUNIT `@Before` method

```java
	private TripService tripService;

	@Before
	public void initialize() {
		tripService = new TestableTripService();
	}
```

Test the deepest branch
--------------------------------
* Create the third test <br>
`should_return_friend_trips_when_users_are_friends()`

```java
	@Test
	public void should_return_friend_trips_when_users_are_friends() throws Exception {
		loggedInUser = REGISTRED_USER;

		User friend = new User();
		friend.addFriend(ANOTHER_USER);
		friend.addFriend(loggedInUser);
		friend.addTrip(TO_BRAZIL);
		friend.addTrip(TO_LONDON);
		
		// When
		List<Trip> friendTrips = tripService.getTripsByUser(friend);
		
		// Then
		assertThat(friendTrips.size(), equalTo(2));
	}
```
* TripService call now `TripServiceDAO.findTripsByUser` and an exception is thrown

```java
	public static List<Trip> findTripsByUser(User user) {
		throw new DependendClassCallDuringUnitTestException("TripDAO should not be invoked on an unit test.");
	}
```

![tripservicedao.png](images/tripservicedao.png)
* A class tested by a unittest should not call other classes, otherwise we have an integrationtest

Introduce another seam method
-----------------------------
* create a seam -> extract method -> `TripServiceDAO.findTripsByUser`

```java
	// TripService
	if (isFriend) {
		tripList = tripsBy(user);
	}

	protected List<Trip> tripsBy(User user) {
		return TripDAO.findTripsByUser(user);
	}
```
```java
	// TripServiceTest
	private class TestableTripService extends TripService {

		@Override
		protected User getLoggedInUser() {
			return loggedInUser;
		}
		
		@Override
		protected List<Trip> tripsBy(User user) {
			return user.trips();
		}
	}
```

All branches are covered with unittests
---------------------------------------
![all-branches-covered.png](images/all-branches-covered.png)


Extract test data from test logic with using builders
-----------------------------------------------------
* Introduce a UserBuilder

```java
private static class UserBuilder {

		private User[] friends = new User[] {};
		private Trip[] trips = new Trip[] {};

		public static UserBuilder aUser() {
			return new UserBuilder();
		}

		public UserBuilder withTrips(Trip... trips) {
			this.trips = trips;
			return this;
		}

		public UserBuilder friendsWith(User... friends) {
			this.friends = friends;
			return this;
		}
		
		public User build() {
			User user = new User();
			addTripsTo(user);
			addFriendsTo(user);
			return user;
		}

		private void addTripsTo(User user) {
			for (Trip trip : trips) {
				user.addTrip(trip);
			}
		}

		private void addFriendsTo(User user) {
			for (User friend : friends) {
				user.addFriend(friend);
			}
		}
	}
```
* Move the builder in a new File `UserBuilder.java`
* import `UserBuilder.aUser()` statically to `aUser()`
* move `loggedInUser = REGISTRED_USER;` to `@Before` block

```java
	// TripServiceTest.java
	
	@Before
	public void initialize() {
		tripService = new TestableTripService();
		loggedInUser = REGISTRED_USER;
	}
	
	@Test(expected = UserNotLoggedInException.class)
	public void should_throw_an_exception_when_user_is_not_logged_in() throws Exception {
		// Given
		loggedInUser = GUEST;

		// When
		tripService.getTripsByUser(UNUSED_USER);
	}

	@Test
	public void should_not_return_any_trips_when_users_are_not_friends() throws Exception {
		// Given
		User friend = aUser()
				.friendsWith(ANOTHER_USER)
				.withTrips(TO_BRAZIL)
				.build();
		
		friend.addFriend(ANOTHER_USER);
		friend.addTrip(TO_BRAZIL);
		
		// When
		List<Trip> friendTrips = tripService.getTripsByUser(friend);
		
		// Then
		assertThat(friendTrips.size(), equalTo(0));
	}
	
	@Test
	public void should_return_friend_trips_when_users_are_friends() throws Exception {
		// Given
		User friend = aUser()
				.friendsWith(ANOTHER_USER, loggedInUser)
				.withTrips(TO_BRAZIL, TO_LONDON)
				.build();
		
		// When
		List<Trip> friendTrips = tripService.getTripsByUser(friend);
		
		// Then
		assertThat(friendTrips.size(), equalTo(2));
	}
```

Refactoring the legacy code with starting on the deepest branch
------------------------------------------------------------
* Legacy code is covered by unittests
* Try to understand why the method is so long
* On the deepest branch we have one liner -> no refactoring needed
* Next candidate for refactoring is the for loop
![deepest-branch-refactoring.png](images/deepest-branch-refactoring.png)
* We found here a [feature envy](https://refactoring.guru/smells/feature-envy). The User should answer if another User is a friend.
* Create a test for the class `User`<br>
`should_inform_when_users_are_not_friends`

```java
	public class UserTest {
	
	private static final User BOB = new User();
	private static final User PAUL = new User();

	@Test
	public void should_inform_when_users_are_not_friends() throws Exception {
		// Given
		User user = aUser()
				.friendsWith(BOB)
				.build();
		
		// When
		boolean isFriend = user.isFriendsWith(PAUL);
		
		// Then
		assertThat(isFriend, equalTo(false));
	}

}
```
* implement the new method `isFriendsWith(User anotherUser)`

```java
	public boolean isFriendsWith(User anotherUser) {
		return false;
	}
```
* Why `return false;` is enough? We should driven by tests. 
* Create a new test for the positive case <br>
`should_inform_when_users_are_friends()`

```java
	@Test
	public void should_inform_when_users_are_friends() throws Exception {
		// Given
		User user = aUser()
				.friendsWith(BOB, PAUL)
				.build();
		
		// When
		boolean isFriend = user.isFriendsWith(PAUL);
		
		// Then
		assertThat(isFriend, equalTo(true));
	}
```
* Change the implementation of `isFriendsWith(User anotherUser)` driven by the second test.

```java
	public boolean isFriendsWith(User anotherUser) {
		return friends.contains(anotherUser);
	}
```

* Eliminate the [feature envy](https://refactoring.guru/smells/feature-envy) on `TripService` class.
* The smallest change is to bring the `isFriend` variable close to for loop
* Instead of the for loop we can call now the `user.isFriendsWith`
* Inline the `user` call in the if clause
* Rename `loggedUser` to `loggedInUser`

```java
	public List<Trip> getTripsByUser(User user) throws UserNotLoggedInException {
		List<Trip> tripList = new ArrayList<Trip>();
		User loggedInUser = getLoggedInUser();
		if (loggedInUser != null) {
			if (user.isFriendsWith(loggedInUser)) {
				tripList = tripsBy(user);
			}
			return tripList;
		} else {
			throw new UserNotLoggedInException();
		}
	}
```
* Now is time to introduce a [guard clause](https://refactoring.guru/replace-nested-conditional-with-guard-clauses).

```java
	public List<Trip> getTripsByUser(User user) throws UserNotLoggedInException {
		User loggedInUser = getLoggedInUser();
		if (loggedInUser == null) {
			throw new UserNotLoggedInException();
		}

		List<Trip> tripList = new ArrayList<Trip>();
		if (user.isFriendsWith(loggedInUser)) {
			tripList = tripsBy(user);
		}
		return tripList;
	}
```
* We have now two blocks.. <br> 1. checking / validating the user <br> 2. getting the trips from friends
* Next we should get rid of this verbose code <br> from

```java
	List<Trip> tripList = new ArrayList<Trip>();
	if (user.isFriendsWith(loggedInUser)) {
		tripList = tripsBy(user);
	}
	return tripList;
```
* to

```java
	if (user.isFriendsWith(loggedInUser)) {
		return tripsBy(user);
	} else {
		return new ArrayList<Trip>();
	}
```
* to

```java
	public List<Trip> getTripsByUser(User user) throws UserNotLoggedInException {
		User loggedInUser = getLoggedInUser();
		if (loggedInUser == null) {
			throw new UserNotLoggedInException();
		}
	
		return user.isFriendsWith(loggedInUser) ? tripsBy(user) : new ArrayList<Trip>();
	}
```
* to

```java
	public List<Trip> getTripsByUser(User user) throws UserNotLoggedInException {
		if (getLoggedInUser() == null) {
			throw new UserNotLoggedInException();
		}

		return user.isFriendsWith(getLoggedInUser()) ? tripsBy(user) : noTrips();
	}

	private List<Trip> noTrips() {
		return emptyList();
	}
```
* and inline `getLoggedInUser()` (tradeoff between verbose and performance)

```java
	public List<Trip> getTripsByUser(User user) throws UserNotLoggedInException {
		if (getLoggedInUser() == null) {
			throw new UserNotLoggedInException();
		}
	
		return user.isFriendsWith(getLoggedInUser()) ? tripsBy(user) : noTrips();
	}
```
* After each modification we ran all tests
* The test should be always green

What is with the design?
------------------------
* `TripService` has a hard dependency to `UserSession`
* `TripService` is part of the model and should not knowing `UserSession`
* We should pass the `User` from the `UserSession` as an argument to `TripService`

```java
	public List<Trip> getTripsByUser(User user, User loggedInUser) throws UserNotLoggedInException {
		if (getLoggedInUser() == null) {
			throw new UserNotLoggedInException();
		}

		return user.isFriendsWith(getLoggedInUser()) ? tripsBy(user) : noTrips();
	}
```
* Adjust the `TripServiceTest` with the second `User` parameter

```java
	@Test(expected = UserNotLoggedInException.class)
	public void should_throw_an_exception_when_user_is_not_logged_in() throws Exception {
		// Given
		loggedInUser = GUEST;

		// When
		tripService.getTripsByUser(UNUSED_USER, GUEST);
	}

	@Test
	public void should_not_return_any_trips_when_users_are_not_friends() throws Exception {
		// Given
		User friend = aUser()
				.friendsWith(ANOTHER_USER)
				.withTrips(TO_BRAZIL)
				.build();
		
		friend.addFriend(ANOTHER_USER);
		friend.addTrip(TO_BRAZIL);
		
		// When
		List<Trip> friendTrips = tripService.getTripsByUser(friend, loggedInUser);
		
		// Then
		assertThat(friendTrips.size(), equalTo(0));
	}
	
	@Test
	public void should_return_friend_trips_when_users_are_friends() throws Exception {
		// Given
		User friend = aUser()
				.friendsWith(ANOTHER_USER, loggedInUser)
				.withTrips(TO_BRAZIL, TO_LONDON)
				.build();
		
		// When
		List<Trip> friendTrips = tripService.getTripsByUser(friend, loggedInUser);
		
		// Then
		assertThat(friendTrips.size(), equalTo(2));
	}
```
* Use the second `User` parameter in `TripService.getTripsByUser` and get rid of `getLoggedInUser()` method

```java
	public class TripService {

	public List<Trip> getTripsByUser(User user, User loggedInUser) throws UserNotLoggedInException {
		if (loggedInUser == null) {
			throw new UserNotLoggedInException();
		}

		return user.isFriendsWith(loggedInUser) ? tripsBy(user) : noTrips();
	}

	private List<Trip> noTrips() {
		return emptyList();
	}

	// Seam
	protected List<Trip> tripsBy(User user) {
		return TripDAO.findTripsByUser(user);
	}

}
```
* Get rid of `loggedInUser` in `TripServiceTest`

```java
public class TripServiceTest {

	private static final User GUEST = null;
	private static final User UNUSED_USER = null;
	private static final User REGISTRED_USER = new User();
	private static final User ANOTHER_USER = new User();
	private static final Trip TO_BRAZIL = new Trip();
	private static final Trip TO_LONDON = new Trip();
	
	private TripService tripService;

	@Before
	public void initialize() {
		tripService = new TestableTripService();
	}
	
	@Test(expected = UserNotLoggedInException.class)
	public void should_throw_an_exception_when_user_is_not_logged_in() throws Exception {
		// When
		tripService.getTripsByUser(UNUSED_USER, GUEST);
	}

	@Test
	public void should_not_return_any_trips_when_users_are_not_friends() throws Exception {
		// Given
		User friend = aUser()
				.friendsWith(ANOTHER_USER)
				.withTrips(TO_BRAZIL)
				.build();
		
		friend.addFriend(ANOTHER_USER);
		friend.addTrip(TO_BRAZIL);
		
		// When
		List<Trip> friendTrips = tripService.getTripsByUser(friend, REGISTRED_USER);
		
		// Then
		assertThat(friendTrips.size(), equalTo(0));
	}
	
	@Test
	public void should_return_friend_trips_when_users_are_friends() throws Exception {
		// Given
		User friend = aUser()
				.friendsWith(ANOTHER_USER, REGISTRED_USER)
				.withTrips(TO_BRAZIL, TO_LONDON)
				.build();
		
		// When
		List<Trip> friendTrips = tripService.getTripsByUser(friend, REGISTRED_USER);
		
		// Then
		assertThat(friendTrips.size(), equalTo(2));
	}

	private class TestableTripService extends TripService {
		
		@Override
		protected List<Trip> tripsBy(User user) {
			return user.trips();
		}
	}
}
```

Get rid of `TripDAO` static method call with an instance method
--------------------------------------------------------------
* What of options we have?

```java
public class TripDAO {
	public static List<Trip> findTripsByUser(User user) {
		throw new DependendClassCallDuringUnitTestException("TripDAO should not be invoked on an unit test.");
	}
}
```
* We can remove the static key word <br>
But then you should fix all classes that use this static method.
* Create `TripDAOTest` class with `should_throw_exception_when_retrieving_user_trips` test.

```java
public class TripDAOTest {
	
	@Test(expected = DependendClassCallDuringUnitTestException.class)
	public void should_throw_exception_when_retrieving_user_trips() throws Exception {
		new TripDAO().tripsBy(new User());
	}

}
```
* Implement the `tripsBy` method

```java
public class TripDAO {

	public static List<Trip> findTripsByUser(User user) {
		throw new DependendClassCallDuringUnitTestException("TripDAO should not be invoked on an unit test.");
	}

	public List<Trip> tripsBy(User user) {
		return new TripDAO().findTripsByUser(user);
	}

}
```
* We need a test in `TripService` that enforce us to inject `TripDAO`
* Add MockitoJunitRunner as test runner
* Get rid of `TestableTripService` and mock `TripDAO` with Mockito

```java
@RunWith(MockitoJUnitRunner.class)
public class TripServiceTest {

	private static final User GUEST = null;
	private static final User UNUSED_USER = null;
	private static final User REGISTRED_USER = new User();
	private static final User ANOTHER_USER = new User();
	private static final Trip TO_BRAZIL = new Trip();
	private static final Trip TO_LONDON = new Trip();
	
	@Mock
	private TripDAO tripDAO;
	
	@InjectMocks
	@Spy
	private TripService tripService;

```
* `should_return_friend_trips_when_users_are_friends` fails now<br>
because the static `TripDAO` method is called.
* Refactor `TripService` to use `TripDAO` instance instead the static method with injecting via Spring `@Autowired` annotation

```java
public class TripService {
	@Autowired
	private TripDAO tripDAO;
	
	public List<Trip> getTripsByUser(User user, User loggedInUser) throws UserNotLoggedInException {
		if (loggedInUser == null) {
			throw new UserNotLoggedInException();
		}

		return user.isFriendsWith(loggedInUser) ? tripsBy(user) : noTrips();
	}

	private List<Trip> noTrips() {
		return emptyList();
	}

	// Seam
	protected List<Trip> tripsBy(User user) {
		return tripDAO.tripsBy(user);
	}

}
```
* After rerun the test the error is changed.<br>
* The `TripService` is using now the injected `TripDAO`.
* We need to configure the mock of `TripDAO` to return `friend.trips()` if the method `tripsBy` is called

```java
	@Test
	public void should_return_friend_trips_when_users_are_friends() throws Exception {
		// Given
		User friend = aUser()
				.friendsWith(ANOTHER_USER, REGISTRED_USER)
				.withTrips(TO_BRAZIL, TO_LONDON)
				.build();
		doReturn(friend.trips()).when(tripDAO).tripsBy(friend);
		
		// When
		List<Trip> friendTrips = tripService.getTripsByUser(friend, REGISTRED_USER);
		
		// Then
		assertThat(friendTrips.size(), equalTo(2));
	}
```

After the refactoring:
----------------------
* TripServiceTest

```java
@RunWith(MockitoJUnitRunner.class)
public class TripServiceTest {

	private static final User GUEST = null;
	private static final User UNUSED_USER = null;
	private static final User REGISTRED_USER = new User();
	private static final User ANOTHER_USER = new User();
	private static final Trip TO_BRAZIL = new Trip();
	private static final Trip TO_LONDON = new Trip();
	
	@Mock
	private TripDAO tripDAO;
	
	@InjectMocks
	@Spy
	private TripService sut;
	
	@Test(expected = UserNotLoggedInException.class)
	public void should_throw_an_exception_when_user_is_not_logged_in() throws Exception {
		// When
		sut.getTripsByUser(UNUSED_USER, GUEST);
	}

	@Test
	public void should_not_return_any_trips_when_users_are_not_friends() throws Exception {
		// Given
		User friend = aUser()
				.friendsWith(ANOTHER_USER)
				.withTrips(TO_BRAZIL)
				.build();
		
		friend.addFriend(ANOTHER_USER);
		friend.addTrip(TO_BRAZIL);
		
		// When
		List<Trip> friendTrips = sut.getTripsByUser(friend, REGISTRED_USER);
		
		// Then
		assertThat(friendTrips.size(), equalTo(0));
	}
	
	@Test
	public void should_return_friend_trips_when_users_are_friends() throws Exception {
		// Given
		User friend = aUser()
				.friendsWith(ANOTHER_USER, REGISTRED_USER)
				.withTrips(TO_BRAZIL, TO_LONDON)
				.build();
		doReturn(friend.trips()).when(tripDAO).tripsBy(friend);
		
		// When
		List<Trip> friendTrips = sut.getTripsByUser(friend, REGISTRED_USER);
		
		// Then
		assertThat(friendTrips.size(), equalTo(2));
	}
}
```

* TripService

```java
public class TripService {
	
	@Autowired
	private TripDAO tripDAO;
	
	public List<Trip> getTripsByUser(User user, User loggedInUser) throws UserNotLoggedInException {
		if (loggedInUser == null) {
			throw new UserNotLoggedInException();
		}

		return user.isFriendsWith(loggedInUser) ? tripsBy(user) : noTrips();
	}

	private List<Trip> noTrips() {
		return emptyList();
	}

	private List<Trip> tripsBy(User user) {
		return tripDAO.tripsBy(user);
	}

}
```
