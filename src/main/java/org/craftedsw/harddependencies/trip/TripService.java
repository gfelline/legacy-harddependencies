package org.craftedsw.harddependencies.trip;

import static java.util.Collections.emptyList;

import java.util.List;

import org.craftedsw.harddependencies.exception.UserNotLoggedInException;
import org.craftedsw.harddependencies.user.User;
import org.springframework.beans.factory.annotation.Autowired;

public class TripService {
	
	@Autowired
	private TripDAO tripDAO;
	
	public List<Trip> getTripsByUser(User user, User loggedInUser) throws UserNotLoggedInException {
		if (loggedInUser == null) {
			throw new UserNotLoggedInException();
		}

		return user.isFriendsWith(loggedInUser) ? tripsBy(user) : noTrips();
	}

	private List<Trip> noTrips() {
		return emptyList();
	}

	private List<Trip> tripsBy(User user) {
		return tripDAO.tripsBy(user);
	}

}
