package org.craftedsw.harddependencies.user;

import static org.craftedsw.harddependencies.trip.UserBuilder.aUser;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import org.junit.Test;

public class UserTest {
	
	private static final User BOB = new User();
	private static final User PAUL = new User();

	@Test
	public void should_inform_when_users_are_not_friends() throws Exception {
		// Given
		User user = aUser()
				.friendsWith(BOB)
				.build();
		
		// When
		boolean isFriend = user.isFriendsWith(PAUL);
		
		// Then
		assertThat(isFriend, equalTo(false));
	}
	
	@Test
	public void should_inform_when_users_are_friends() throws Exception {
		// Given
		User user = aUser()
				.friendsWith(BOB, PAUL)
				.build();
		
		// When
		boolean isFriend = user.isFriendsWith(PAUL);
		
		// Then
		assertThat(isFriend, equalTo(true));
	}

}
