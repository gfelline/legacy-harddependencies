package org.craftedsw.harddependencies.trip;

import org.craftedsw.harddependencies.exception.DependendClassCallDuringUnitTestException;
import org.craftedsw.harddependencies.user.User;
import org.junit.Test;

public class TripDAOTest {
	
	@Test(expected = DependendClassCallDuringUnitTestException.class)
	public void should_throw_exception_when_retrieving_user_trips() throws Exception {
		new TripDAO().tripsBy(new User());
	}

}
