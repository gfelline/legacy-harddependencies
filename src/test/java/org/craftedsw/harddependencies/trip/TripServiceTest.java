package org.craftedsw.harddependencies.trip;

import static org.craftedsw.harddependencies.trip.UserBuilder.aUser;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.doReturn;

import java.util.List;

import org.craftedsw.harddependencies.exception.UserNotLoggedInException;
import org.craftedsw.harddependencies.user.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class TripServiceTest {

	private static final User GUEST = null;
	private static final User UNUSED_USER = null;
	private static final User REGISTRED_USER = new User();
	private static final User ANOTHER_USER = new User();
	private static final Trip TO_BRAZIL = new Trip();
	private static final Trip TO_LONDON = new Trip();
	
	@Mock
	private TripDAO tripDAO;
	
	@InjectMocks
	@Spy
	private TripService sut;
	
	@Test(expected = UserNotLoggedInException.class)
	public void should_throw_an_exception_when_user_is_not_logged_in() throws Exception {
		// When
		sut.getTripsByUser(UNUSED_USER, GUEST);
	}

	@Test
	public void should_not_return_any_trips_when_users_are_not_friends() throws Exception {
		// Given
		User friend = aUser()
				.friendsWith(ANOTHER_USER)
				.withTrips(TO_BRAZIL)
				.build();
		
		friend.addFriend(ANOTHER_USER);
		friend.addTrip(TO_BRAZIL);
		
		// When
		List<Trip> friendTrips = sut.getTripsByUser(friend, REGISTRED_USER);
		
		// Then
		assertThat(friendTrips.size(), equalTo(0));
	}
	
	@Test
	public void should_return_friend_trips_when_users_are_friends() throws Exception {
		// Given
		User friend = aUser()
				.friendsWith(ANOTHER_USER, REGISTRED_USER)
				.withTrips(TO_BRAZIL, TO_LONDON)
				.build();
		doReturn(friend.trips()).when(tripDAO).tripsBy(friend);
		
		// When
		List<Trip> friendTrips = sut.getTripsByUser(friend, REGISTRED_USER);
		
		// Then
		assertThat(friendTrips.size(), equalTo(2));
	}
}
